%!TEX root = report.tex
This section describes the different experiments we conducted. For every experiment we used 20.000 training episodes and 5.000 episodes for testing. We opted to terminate the game if the number of steps exceeded a limited of 500. We found empirically that 500 episodes should be enough, but this is not included in the paper.

\subsection{Experiment 1 (Random agent vs. single agent)}\label{ss:exp:1}
Experiment one uses case one that is described in \cref{ss:game:config}. The hunter is given a view depth of 2 and is either a \textit{Q-learner} or a randomly moving hunter. The results of the training and testing can be seen in \cref{fig:exp:1:training} and \cref{fig:exp:1:testing} respectively.
\begin{figure}
	\centering
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./img/random_vs_learning_single_training}
		\caption{Final training values for the Q-Learner and random hunter are 76.01 and 18.94 respectively}
		\label{fig:exp:1:training}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./img/random_vs_learning_single_testing}
		\caption{Final testing values for the Q-Learner and random hunter are 77.83 and 13.92 respectively}
		\label{fig:exp:1:testing}
	\end{subfigure}	
	\caption{The running average of the training and testing episodes, shown for the random and Q-Learning hunters.}
	\label{fig:exp:1}
\end{figure}

The graphs in \cref{fig:exp:1} show running averages of the number of steps needed for the hunter to capture the prey. Because the hunter and prey spawn randomly in the world there can be a lot of variation in the number of steps. A running average in this case results in a qualitatively better plot then plotting the number of steps for each episode. We see that the random hunter averages around 80 in both training and testing, compared to the \textit{Q-learner} only using 13.92 steps on average in the testing phase. When looking at the training graph we see that the running average of the \textit{Q-learner}  drops quickly to around 20 steps after 10.000 episodes. The final average during training of the \textit{Q-learner} is 18.94. The difference between the training and testing is explained by the the fact the the average during training is influenced by the training episodes, in the beginning, that need more steps. From these results we conclude that our implementation learns and performs better than random.

\subsection{Experiment 2 (Influence of increasing view depth)}\label{ss:exp:2}
Experiment two looks at the influence of increasing the view depth of the hunters. This experiment uses configuration case one from \cref{ss:game:config}. We expect the better performance, i.e., testing average, from a hunter with more vision. Although increasing view depth might cause the performance convergence to take longer because of the increase in state space. The results for training and testing are shown in \cref{fig:exp:2:training} and \cref{fig:exp:2:testing} respectively.

\begin{figure}
	\centering
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./img/view_depth_training}
		\caption{Final training values for view depth 2 to 4 are 18.94, 11.46, and 9.52 respectively}
		\label{fig:exp:2:training}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./img/view_depth_testing}
		\caption{Final testing values for view depth 2 to 4 are 13.92, 8.75, and 7.55 respectively}
		\label{fig:exp:2:testing}
	\end{subfigure}	
	\caption{The running average of the training and testing episodes, shown for a single Q-Learning hunter with different view depths (vd).}
	\label{fig:exp:2}
\end{figure}

The results for view depth 2 are the same in \cref{fig:exp:1}. With view depth 3 and 4 we see that the hunter is actually trained quicker compared to a view depth of 2. With a higher view depth the hunter sees the prey earlier and more often, resulting in faster training and a lower average. The increase in performance is quite substantial going from 13.92 steps to 8.75 steps in the testing phase with only an increase of one in the view depth, and resulting in only 7.55 steps needed to catch the prey with a view depth of 4. By increasing the view depth further we expect that the performance would not significantly increase any further.

\subsection{Experiment 3 (Scouts)}\label{ss:exp:3}
Experiment 3 looks at multiple agents hunting a prey using game configuration case 2 from \cref{ss:game:config}. Looking at the effect of scouts on hunter performance, we expect that using scouts that can spot the prey will have similar effects as increasing the view depth of the hunter, and thus decreasing the amount of steps needed to capture the prey. Either one, two or no scouts are used, with the no-scout condition being exactly the same as experiment 1 (\cref{ss:exp:1}). The results of training and testing are shown in \cref{fig:exp:3:training} and \cref{fig:exp:3:testing} respectively.

\begin{figure}
	\centering
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./img/scout_training_b}
		\caption{Final training averages are 18.11, 20.44, and 20.60 for 0, 1 and 2 scouts respectively}
		\label{fig:exp:3:training}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./img/scout_testing_b}
		\caption{Final testing averages are 14.61, 13.37 and 14.25 for 0, 1 and 2 scouts respectively}
		\label{fig:exp:3:testing}
	\end{subfigure}	
	\caption{The running average of the training and testing episodes, shown for a single Q-Learning hunter with 0, 1 or 2 scouts.}
	\label{fig:exp:3}
\end{figure}

When looking at the training plot we see almost no difference between the different number of scouts. The hunter without scouts is again the same as  \cref{ss:exp:1}, only this time the data for the plots was obtained from a different run of experiment one, hence the slightly deviating values. Interesting is the slower training curve for hunter with a scout compared to without a scout. The state space of a hunter with one scout increases slightly compared to the state space of a hunter without scouts. With no scout the state of the hunter can only contain a prey when it is at most two steps away from the hunter, with a scout the prey can occur at any place relative to the hunter. This probably explains the difference in training slopes for the different number of scouts and also why having 1 or 2 scouts does not matter in that sense. 

The testing data shows no real difference between any number of scouts. Adding a scout seems to have no effect on the number of steps needed to catch the prey after training. 

\subsection{Experiment 4 (coop vs. single tasks)}\label{ss:exp:4}
Experiment 4 uses game configuration case 3, from \cref{ss:game:config}. The hunters have to work together to capture the prey and do that with  communicating with each other. Communicating hunters tell each other when they see the prey, which means that if one hunter can see the prey, both hunters know the location of the prey relative to themselves. In that sense two hunters together both have the function of scout. The experiment has two tasks, in one task the hunters need to cooperate to capture the prey, i.e., they both have to be adjacent to the prey. In the other task both hunters can capture the prey independently of each other. The results of training and test can be seen in \cref{fig:exp:4:training} and \cref{fig:exp:4:testing} respectively.

\begin{figure}
	\centering
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./img/task_training}
		\caption{Final training averages are 155.22, and 18.92 for the cooperative task and the single task respectively}
		\label{fig:exp:4:training}
	\end{subfigure}
	\begin{subfigure}{0.48\textwidth}
		\centering
		\includegraphics[width=\textwidth]{./img/task_testing}
		\caption{Final testing averages are 64.69, 12.45 for the cooperative task and the single task respectively}
		\label{fig:exp:4:testing}
	\end{subfigure}	
	\caption{The running average of the training and testing episodes, shown for two hunters working cooperatively and independently of each other.}
	\label{fig:exp:4}
\end{figure}

For the coop task we see that after 20.000 training episodes there is still improvement in the average steps, but when using 40.000 training episodes we get the same testing average indicating that 20.000 training steps is enough for convergence. Working together to catch the prey is a much harder task, but it is clear that there is some degree of learning. 

Single task averages in this experiment resemble averages from experiment \cref{ss:exp:1}. The testing average of 12.45 is slightly lower, which is what we expect because two hunters should be better at catching a prey than one, when only one hunter has to be at a prey to catch it.


