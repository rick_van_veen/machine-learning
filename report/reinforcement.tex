%!TEX root = report.tex
In reinforcement learning the agent, a hunter in our case, makes a decision based on the current state of the environment. A state at time \textit{t} that contains all relevant information of previous states is considered to have the \textit{Markov property}. All the information that is important for the next state is present in the current state. The environment dynamics can then be defined as \cite{sutton1998}
\begin{equation}
\textbf{\textit{Pr}}(R_{t+1} = r, S_{t+1} = s' | S_t, A_t)
\end{equation}
where \textit{R} is the reward, \textit{S} the state and \textit{A} the action, meaning that the next state and reward can be determined given the current state and an action taken in that state. A reinforcement learning task that satisfies this \textit{Markov property} is called a \textit{Markov decision process} or commonly abbreviated to \textit{MDP}. If the states and action space in that reinforcement learning task is also finite, which is the case in our hunting game, then it is also called a \textit{finite MDP}, which is what most of the applications of reinforcement learning are. 

A \textit{finite MDP} has a set of actions and a number of states and given a state \textit{s} and an action \textit{a} the probability of the next state \textit{s'} is represented by
\begin{equation}
\label{eq:5}
\textbf{\textit{Pr}}(S_{t+1} =  s' | S_t = s, A_t = a)
\end{equation}
Also, given a state \textit{s} and an action \textit{a} together with the next state \textit{s'} the expected reward is
\begin{equation}
\label{eq:6}
r(s,a,s') = \mathbb{E}\big[R_{t+1} | S_t = s, A_t = a, S_{t+1} = s' \big]
\end{equation}

Given equation \ref{eq:5} and \ref{eq:6} the optimal policy in a \textit{finite MDP} can be determined using the Bellman equation (here for \textit{Q-learning})
\begin{equation}
\label{eq:7}
q_*(s,a) = \mathbb{E}\big[R_{t+1} + \gamma \max_{a'}q_*(S_{t+1},a') | S_t = s, A_t = a\big]
\end{equation}

Finding the optimal policy only requires to look one step ahead, determine at each state \textit{s} the Bellman equation \ref{eq:7} gives one or more actions with the maximum value. These actions represent optimal actions in that state, which are used in \textit{Q-learning}, which will be explained next.

%\todo[inline]{Dynamic programming}
%
%\subsection{Dynamic programming}
%Reinforcement learning is an technique that approximates the optimal control technique dynamic programming. 
%it divides the problem in smaller problems. Then it tries to `solve' the smaller problems and save the solution to these smaller problems. This way when it may encounter the same small problems multiple times and it may know the solution to a partial problem from a previous iteration. \todo{this only better}
%

\subsection{Q-Learning}
Reinforcement learning can be implemented in different ways, but one of the most common methods is \textit{Q-learning} \cite{watkins1992}. \textit{Q-learning} initially gives each state-action pair the same Q-value. This Q-value gets updated every time a state-action pair is encountered and represents the expected discounted reward the agent will receive by doing that action in the corresponding state. 

Each state-action pair, which an agent encounters, can be considered an episode, at episode \textit{n, Q-learning} consists of the following steps:
\begin{enumerate}
  \setlength{\itemsep}{0pt}
  \setlength{\parskip}{0pt}
\item Observe the current state $s_n$ 
\item Select and perform an action $a_n$ based on current Q-value estimates of all actions possible in that state $(Q(s_n,a))$ 
\item Observe the next state $s_{n+1}$
\item Receive a reward $r_n$
\item Adjust the Q-value of the state-action pair $Q(s_n,a_n)$ using a learning factor $\alpha$.
\end{enumerate}
At step 2, different policies can be used to select the action based on the Q-values. A common policy which is used in this paper is the $\epsilon$-greedy policy, which selects an action based on
\begin{equation}
a_n = \max_{a'}(Q(x_n,a'))
\end{equation}
taking the action with the highest expected reward, with a $(1-\epsilon)$ chance of taking a random action instead. The parameter $\epsilon$ can be between 0 and  1 is usually set to a value of 0.1. A greater value for $\epsilon$ results in higher exploration because then more often a random decision is being made. We use the common value of 0.1 because we want the hunter to follow the prey once this is in sight, and not wander off exploring. 

After the next state has been observed and the reward is known for that state the Q-values are adjusted in step 5 according to the following update-rule:
\begin{equation}
Q(s_n,a_n) = Q(s_n,a_n) + \alpha \Big[ r + \gamma \max_{a'} Q(s_{n+1},a') - Q(s_n,a_n) \Big]
\end{equation}
Which takes the Q-value of the last state-action pair $(s_n,a_n)$ and adds the learning rate $\alpha$ (which is set between 0 and 1) multiplied with the sum of reward \textit{r} and the max Q-value in the next state minus the current Q-value of the state-action pair. The max Q-value of the next state is multiplied with a discounting factor $\gamma$ (between 0 and 1) to account for the importance of sooner versus later rewards. The learning rate is optimal for \textit{Q-learning} between 0.7 and 0.9, so we chose a value of 0.8. The discount factor is set at 0.9, because we want to take into account the possible rewards in the next state. Similar values are also used in other \textit{Q-learning} experiments. 

After \textit{n}-episodes the Q-values of the agent adjust towards the optimal decision policy where the amount of steps needed to achieve this optimal policy depends on the size of the world, or in other words the amount of state-action pairs a certain world has. 