## Machine Learning ##

Learning is an essential part of intelligence. It makes it possible to cope with uncertain environments or domains about which one has insufficient knowledge to completely model it. Machine learning algorithm are usually data-driven which means that the algorithms learn functions mapping inputs to desired outputs based on example data.

In this course we will treat a wide variety of machine learning algorithms such as decision trees, neural networks, support vector machines, and reinforcement learning algorithms.

There will be a practicum where students will implement their own machine learning system and they will write a report about this system and the obtained results. Furthermore, there will be an examination at the end of the course.

# Mark #

The examination will count for 50% of the final mark and the practical report also for 50%. The grade of the exam needs to be higher or equal to 5.0 in order to pass this course.

# Planning #

* 0.2 **CleanCode**
	- Refactor
* 0.3 **Sensations**
	- Sensations
* 0.4 **Fair Game**
	- Moving 'Pray'
* 0.5 **Teamwork**
	- Multi agents