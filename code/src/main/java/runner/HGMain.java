package runner;

import config.HGConfig;
import experiments.HGExperiment;
import experiments.HGIExperiment;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

import java.io.IOException;

public class HGMain {

    public static void main(String[] args) throws IOException{
        /** ### DEBUG MODE ###
         *
         * Uncomment to generate the same results
         *
         **/

        //long seed = 31415;
        //RandomFactory.seedMapped(0,seed);
        //RandomFactory.seedDefault(seed);

        OptionParser parser = parseArgs();
        OptionSet os = null;
        boolean ranSomething = false;
        try{os = parser.parse(args);}
        catch(Exception e){
            parser.printHelpOn( System.out );
            return;
        }

        if(os.hasArgument("h") || args.length == 0){
            parser.printHelpOn( System.out );
            return;
        }

        int expermimentNumber = (Integer)os.valueOf("n") - 1;
        System.out.println("Experiment " + (expermimentNumber + 1) + " (index " + expermimentNumber + ")\n");
        HGIExperiment experiment = new HGExperiment(HGConfig.experiments[expermimentNumber]);

        if(os.has("e")) {
            experiment.runExperiment();
            ranSomething = true;
        }

        if(os.has("l")) {
            experiment.showTrainingEpisodes();
            ranSomething = true;
        }

        if(os.has("t")) {
            experiment.showTestingEpisodes();
            ranSomething = true;
        }

        if(!ranSomething){
            parser.printHelpOn( System.out );
        }
    }

    private static OptionParser parseArgs() {
        OptionParser parser = new OptionParser() {
            {
                accepts( "n" )
                        .withOptionalArg()
                        .ofType( Integer.class )
                        .describedAs( "The number of the experiment: [1 -" + HGConfig.experiments.length + "]" )
                        .defaultsTo( 1 );

                accepts( "e" )
                        .withOptionalArg()
                        .ofType( Boolean.class )
                        .describedAs( "Run the experiment" )
                        .defaultsTo( false );

                accepts( "t" )
                        .withOptionalArg()
                        .ofType( Boolean.class )
                        .describedAs( "Show testing episodes" )
                        .defaultsTo( false );

                accepts( "l" )
                        .withOptionalArg()
                        .ofType( Boolean.class )
                        .describedAs( "Show learning episodes" )
                        .defaultsTo( false );
                accepts( "h" )
                        .withOptionalArg()
                        .ofType( Boolean.class )
                        .describedAs( "Show the help screen" )
                        .defaultsTo( false );
            }
        };
        return parser;
    }
}
