package world;

import agents.moveables.HGAMoveableObject;
import agents.prey.HGDynamicPrey;
import agents.prey.HGStaticPrey;
import agents.scout.HGDynamicScout;
import agents.scout.HGStaticScout;
import agents.types.HGHunterType;
import agents.types.HGPreyType;
import agents.types.HGScoutType;
import burlap.behavior.stochasticgames.PolicyFromJointPolicy;
import burlap.behavior.stochasticgames.agents.RandomSGAgent;
import burlap.behavior.stochasticgames.agents.maql.MultiAgentQLearning;
import burlap.behavior.stochasticgames.madynamicprogramming.backupOperators.MaxQ;
import burlap.behavior.stochasticgames.madynamicprogramming.policies.EGreedyJointPolicy;
import burlap.oomdp.statehashing.SimpleHashableStateFactory;
import burlap.oomdp.stochasticgames.*;
import config.HGConfig;
import domain.HGJointRewardFunctionMultie;
import domain.HGTerminalFunction;
import state.HGStateAbstraction;
import state.HGStateGenerator;

import java.util.List;

public class HGWorldGenerator implements WorldGenerator {

    private SGDomain domain;
    private HGTerminalFunction tf;
    private JointReward jf;
    private HGStateGenerator stateGenerator;
    private HGStateAbstraction stateAbstraction;

    private List<HGHunterType> hunterTypes;
    private List<HGPreyType> preyTypes;
    private List<HGScoutType> scoutTypes;

    public HGWorldGenerator(SGDomain domain, JointReward jf, HGTerminalFunction tf, HGStateGenerator stateGenerator, HGStateAbstraction stateAbstraction, List<HGHunterType> hunterTypes, List<HGPreyType> preyTypes, List<HGScoutType> scoutTypes){
        this.domain = domain;
        this.jf = jf;
        this.tf = tf;
        this.stateGenerator = stateGenerator;
        this.stateAbstraction = stateAbstraction;

        this.hunterTypes = hunterTypes;
        this.preyTypes = preyTypes;
        this.scoutTypes = scoutTypes;
    }

    public HGWorld generateWorld() {
        HGWorld world = new HGWorld(domain, jf, tf, stateGenerator, stateAbstraction);

        this.addHunters(world, this.hunterTypes);
        this.addPreys(world, this.preyTypes);
        this.addScouts(world, this.scoutTypes);

        return world;
    }

    private void addHunters(HGWorld world, List<HGHunterType> HunterTypes) {
        for(HGHunterType hunterType : HunterTypes) {
            this.addHunter(world, hunterType);
        }
    }

    private  void addHunter(HGWorld world, HGHunterType hunterType) {
        SGAgent hunter = hunterType.isRandom()?generateRandomHunter():generateQHunter(hunterType);
        SGAgentType agentType = new SGAgentType(HGConfig.Classes.HUNTER,
                domain.getObjectClass(HGConfig.Classes.HUNTER),
                domain.getAgentActions());
        hunter.joinWorld(world, agentType);
    }

    private SGAgent generateQHunter(HGHunterType hunterType){
        MultiAgentQLearning hunter = new MultiAgentQLearning(
                this.domain,
                hunterType.getDiscount(),
                hunterType.getLearningRate(),
                new SimpleHashableStateFactory(),
                hunterType.getInitialQValue(),
                new MaxQ(),
                hunterType.isQueryOtherAgentsQValues()
        );

        hunter.setLearningPolicy(
                new PolicyFromJointPolicy(
                        new EGreedyJointPolicy(hunterType.getEpsilon())
                )
        );
        return hunter;
    }

    private SGAgent generateRandomHunter(){
        return new RandomSGAgent();
    }


    private void addScouts(HGWorld world, List<HGScoutType> scoutTypes){
        for (HGScoutType scoutType : scoutTypes) {
            addScout(world, scoutType);
        }
    }

    private void addPreys(HGWorld world, List<HGPreyType> preyTypes) {
        for(HGPreyType preyType : preyTypes) {
            addPrey(world, preyType);
        }
    }

    // TODO: Java reflection? Define function on the world not put them in the lists here.
    private void addPrey(HGWorld world, HGPreyType preyType) {
        HGAMoveableObject prey = null;
        int numMoveableObjects = world.movableObjects.size();
        String preyClass = preyType.getClassName();
        if (preyClass.equals(HGDynamicPrey.class.getName())) {
            prey = new HGDynamicPrey("prey" + numMoveableObjects,HGConfig.Classes.PREY);
        } else if (preyClass.equals(HGStaticPrey.class.getName())) {
            prey = new HGStaticPrey("prey" + numMoveableObjects,HGConfig.Classes.PREY);
        }
        if(prey != null) {
            world.movableObjects.add(prey);
        } else {
            System.out.println("Prey of type class: " + preyClass + " does not exist.");
        }
    }
    // TODO: Java reflection? Define function on the world not put them in the lists here.
    private void addScout(HGWorld world, HGScoutType scoutType) {
        HGAMoveableObject scout = null;
        int numMoveableObjects = world.movableObjects.size();
        String scoutClass = scoutType.getClassName();
        if (scoutClass.equals(HGDynamicScout.class.getName())) {
            scout = new HGDynamicScout("scout" + numMoveableObjects, HGConfig.Classes.SCOUT);
        } else if (scoutClass.equals(HGDynamicScout.class.getName())) {
            scout = new HGStaticScout("scout" + numMoveableObjects, HGConfig.Classes.SCOUT);
        }
        if(scout != null) {
            world.movableObjects.add(scout);
        } else {
            System.out.println("Scout of type class: " + scoutClass + " does not exist.");
        }
    }
}
