package world;

import agents.moveables.HGAMoveableObject;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.datastructures.HashedAggregator;
import burlap.debugtools.DPrint;
import burlap.oomdp.auxiliary.StateAbstraction;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.*;
import state.HGIStateAbstracion;
import state.HGStateGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HGWorld extends World{

    protected HGIStateAbstracion abstractionForAgents;
    protected HGStateGenerator initialStateGenerator;

    public List<HGAMoveableObject> movableObjects;
    private boolean isTrainingGame = true;

    public HGWorld(SGDomain domain, JointReward jr, TerminalFunction tf, State initialState) {
        super(domain, jr, tf, initialState);
    }

    public HGWorld(SGDomain domain, JointReward jr, TerminalFunction tf, SGStateGenerator sg) {
        super(domain, jr, tf, sg);
    }

    public HGWorld(SGDomain domain, JointReward jr, TerminalFunction tf, SGStateGenerator sg, HGIStateAbstracion abstractionForAgents) {
        super(domain, jr, tf, sg);
        this.abstractionForAgents = abstractionForAgents;
    }

    @Override
    protected void init(SGDomain domain, JointActionModel jam, JointReward jr, TerminalFunction tf, SGStateGenerator sg, StateAbstraction abstractionForAgents){
        this.domain = domain;
        this.worldModel = jam;
        this.jointRewardModel = jr;
        this.tf = tf;
        this.initialStateGenerator = (HGStateGenerator)sg;
        this.movableObjects = new ArrayList<HGAMoveableObject>();

        agents = new ArrayList<SGAgent>();
        agentsByType = new HashMap<SGAgentType, List<SGAgent>>();
        this.agentDefinitions = new HashMap<String, SGAgentType>();

        agentCumulativeReward = new HashedAggregator<String>();

        worldObservers = new ArrayList<WorldObserver>();

        debugId = 284673923;
    }

    public GameAnalysis runTrainingGame(int maxStages){
        this.isTrainingGame = true;
        return this.runGame(maxStages);
    }

    public GameAnalysis runTestingGame(int maxStages){
        this.isTrainingGame = false;
        return this.runGame(maxStages);
    }

    @Override
    public GameAnalysis runGame(int maxStages){
        for(SGAgent a : agents){
            a.gameStarting();
        }

        // Add prey here and thus to state generator.
        currentState = initialStateGenerator.generateState(agents, movableObjects);
        this.currentGameRecord = new GameAnalysis(currentState);
        this.isRecordingGame = true;
        int t = 0;

        for(WorldObserver wob : this.worldObservers){
            wob.gameStarting(this.currentState);
        }

        while(!tf.isTerminal(currentState) && t < maxStages){
            this.runStage();
            t++;
        }

        for(SGAgent a : agents){
            a.gameTerminated();
        }

        for(WorldObserver wob : this.worldObservers){
            wob.gameEnding(this.currentState);
        }

        DPrint.cl(debugId, currentState.getCompleteStateDescription());

        this.isRecordingGame = false;

        return this.currentGameRecord;
    }

    @Override
    public void runStage(){
        if(tf.isTerminal(currentState)){
            return;
        }

        // Update Hunters.
        JointAction ja = new JointAction();
        Map<String, State> abstractedStates = new HashMap<String, State>();

        for(SGAgent a : agents){
            State abstractedCurrent = abstractionForAgents.abstraction(currentState, a.getAgentName());
            abstractedStates.put(a.getAgentName(), abstractedCurrent);
            ja.addAction(a.getAction(abstractedCurrent));
        }
        this.lastJointAction = ja;

        DPrint.cl(debugId, ja.toString());

        //now that we have the joint action, perform it
        State sp = worldModel.performJointAction(currentState, ja);
        // Perform actions of prey and scouts.
        for(HGAMoveableObject moveableObject : movableObjects) {
            moveableObject.performAction(sp);
        }

        Map<String, Double> jointReward = jointRewardModel.reward(currentState, ja, sp);

        DPrint.cl(debugId, jointReward.toString());

        //index reward
        for(String aname : jointReward.keySet()){
            double r = jointReward.get(aname);
            agentCumulativeReward.add(aname, r);
        }

        if(this.isTrainingGame) {
            //tell all the agents about it
            for (SGAgent a : agents) {
                State abstractedPrime = this.abstractionForAgents.abstraction(sp, a.getAgentName());
                a.observeOutcome(abstractedStates.get(a.getAgentName()), ja, jointReward, abstractedPrime, tf.isTerminal(sp));
            }
        }
        //tell observers
        for(WorldObserver o : this.worldObservers){
            o.observe(currentState, ja, jointReward, sp);
        }

        //update the state
        currentState = sp;

        //record events
        if(this.isRecordingGame){
            this.currentGameRecord.recordTransitionTo(this.lastJointAction, this.currentState, jointReward);
        }
    }
}
