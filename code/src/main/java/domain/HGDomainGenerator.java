package domain;

import burlap.oomdp.auxiliary.DomainGenerator;
import burlap.oomdp.core.Attribute;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.ObjectClass;
import burlap.oomdp.stochasticgames.SGDomain;
import domain.actions.HGAction;
import domain.propfunc.HGHunterAtPrey;
import domain.propfunc.HGHunterNearPrey;
import domain.propfunc.HGPreyIsCapturedCoop;
import config.HGConfig;
import domain.propfunc.HGPreyisCapturedSingle;

public class HGDomainGenerator implements DomainGenerator {

    private Attribute xAttribute;
    private Attribute yAttribute;

    // The used terminal function
    private String tf;

    public HGDomainGenerator(String tf){
        this.tf = tf;
    }

    private void generateAttributes(SGDomain domain) {
        this.xAttribute = new Attribute(domain,
                HGConfig.Attributes.XPOS, Attribute.AttributeType.INT);

        this.xAttribute.setLims(HGConfig.WorldSettings.MINDIM,
                HGConfig.WorldSettings.MAXDIM);

        this.yAttribute = new Attribute(domain,
                HGConfig.Attributes.YPOS, Attribute.AttributeType.INT);

        this.yAttribute.setLims(HGConfig.WorldSettings.MINDIM,
                HGConfig.WorldSettings.MAXDIM);
    }

    private void generateHunter(SGDomain domain) {
        ObjectClass hunterClass = new ObjectClass(domain, HGConfig.Classes.HUNTER);

        hunterClass.addAttribute(this.xAttribute);
        hunterClass.addAttribute(this.yAttribute);
    }

    private void generateScout(SGDomain domain) {
        ObjectClass scoutClass = new ObjectClass(domain, HGConfig.Classes.SCOUT);

        scoutClass.addAttribute(this.xAttribute);
        scoutClass.addAttribute(this.yAttribute);
    }

    private void generatePrey(SGDomain domain) {
        ObjectClass preyClass = new ObjectClass(domain, HGConfig.Classes.PREY);

        preyClass.addAttribute(this.xAttribute);
        preyClass.addAttribute(this.yAttribute);
    }

    private void generateActions(SGDomain domain) {
        new HGAction(domain, HGConfig.Actions.NORTH);
        new HGAction(domain, HGConfig.Actions.EAST);
        new HGAction(domain, HGConfig.Actions.SOUTH);
        new HGAction(domain, HGConfig.Actions.WEST);
    }

    private void generatePropositionalFunctions(SGDomain domain) {
        new HGHunterAtPrey(HGConfig.Propositional.HUNTERATPREY,
                domain);

        new HGHunterNearPrey(HGConfig.Propositional.HUNTERNEARPREY,
                domain);

//        new HGStaticPrey(HGConfig.Propositional.STATICPREY,
//                domain);

        new HGPreyIsCapturedCoop(HGConfig.Propositional.PREYISCAPTUREDCOOP,
                domain,this.tf);

        new HGPreyisCapturedSingle(HGConfig.Propositional.PREYISCAPTUREDSINGLE,
                domain,this.tf);
    }

    public Domain generateDomain() {
        SGDomain domain = new SGDomain();

        generateAttributes(domain);
        generateHunter(domain);
        generatePrey(domain);
        generateScout(domain);
        generateActions(domain);
        generatePropositionalFunctions(domain);

        domain.setJointActionModel(new HGJointActionModel());

        return domain;
    }
}
