package domain;


import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.JointReward;
import config.HGConfig;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HGJointRewardFunctionSingle implements JointReward{

    TerminalFunction tf;

    public HGJointRewardFunctionSingle(TerminalFunction tf) {
        this.tf = tf;
    }

    public Map<String, Double> reward(State s, JointAction ja, State sp) {
        Map<String, Double> rewards = new HashMap<String, Double>();

        // TODO: Magic numbers. Check the next state not the current state.
        double reward = this.tf.isTerminal(sp) ? 10.0 : -1.0;
        // Default reward for the hunters.
        List<ObjectInstance> hunters = sp.getObjectsOfClass(HGConfig.Classes.HUNTER);
        for(ObjectInstance hunter : hunters) {
                rewards.put(hunter.getName(), reward);
        }
        return rewards;
    }
}
