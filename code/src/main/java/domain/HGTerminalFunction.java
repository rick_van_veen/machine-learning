package domain;

import burlap.oomdp.auxiliary.common.SinglePFTF;
import burlap.oomdp.core.PropositionalFunction;

// Class SinglePFTF checks if the propositional function is true for any 'Grounded' version, meaning
// if the function is defined for the class hunter and prey it will check if the function is true for
// all combinations of hunters and prey in the game.
public class HGTerminalFunction extends SinglePFTF {

    public HGTerminalFunction(PropositionalFunction pf) {
        super(pf);
    }
}
