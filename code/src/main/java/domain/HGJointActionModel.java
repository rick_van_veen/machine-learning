package domain;

import burlap.oomdp.core.TransitionProbability;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.JointActionModel;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import config.HGConfig;

import java.util.List;

public class HGJointActionModel extends JointActionModel{

    @Override
    public List<TransitionProbability> transitionProbsFor(State s, JointAction ja) {
        return deterministicTransitionProbsFor(s, ja);
    }

    @Override
    protected State actionHelper(State s, JointAction ja) {
        List<GroundedSGAgentAction> actions = ja.getActionList();

        for (GroundedSGAgentAction action : actions) {
            String agentAction = action.actionName();

            ObjectInstance actingAgent = s.getObject(action.actingAgent);
            int xPos = actingAgent.getIntValForAttribute(HGConfig.Attributes.XPOS);
            int yPos = actingAgent.getIntValForAttribute(HGConfig.Attributes.YPOS);

            if (agentAction.equals(HGConfig.Actions.NORTH)) {
                actingAgent.setValue(HGConfig.Attributes.YPOS, ++yPos);
            } else if(agentAction.equals(HGConfig.Actions.EAST)) {
                actingAgent.setValue(HGConfig.Attributes.XPOS, ++xPos);
            } else if(agentAction.equals(HGConfig.Actions.SOUTH)) {
                actingAgent.setValue(HGConfig.Attributes.YPOS, --yPos);
            } else { // "west"
                actingAgent.setValue(HGConfig.Attributes.XPOS, --xPos);
            }
        }
        return s;
    }
}
