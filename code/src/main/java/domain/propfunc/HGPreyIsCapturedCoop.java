package domain.propfunc;

import burlap.oomdp.core.Domain;
import burlap.oomdp.core.GroundedProp;
import burlap.oomdp.core.PropositionalFunction;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import config.HGConfig;

import java.util.List;

public class HGPreyIsCapturedCoop extends PropositionalFunction{

    private String propFunction;

    public HGPreyIsCapturedCoop(String name, Domain domain, String propFunction) {
        super(name, domain, new String(HGConfig.Classes.PREY));
        this.propFunction = propFunction;
    }

    @Override
    public boolean isTrue(State s, String... params) {
        List<ObjectInstance> hunters = s.getObjectsOfClass(HGConfig.Classes.HUNTER);

        PropositionalFunction hunterAtPreyProp = this.domain.getPropFunction(this.propFunction);
        GroundedProp hunterAtPrey = null;
        for (ObjectInstance hunter : hunters) {
            hunterAtPrey = new GroundedProp(hunterAtPreyProp,
                    new String[]{hunter.getName(), params[0]});

            if(!hunterAtPrey.isTrue(s)) {
                return false;
            }
        }
        return true;
    }
}
