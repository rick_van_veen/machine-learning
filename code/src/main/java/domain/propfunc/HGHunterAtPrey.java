package domain.propfunc;

import burlap.oomdp.core.Domain;
import burlap.oomdp.core.PropositionalFunction;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import config.HGConfig;

public class HGHunterAtPrey extends PropositionalFunction {
    public HGHunterAtPrey(String name, Domain domain) {
        super(name, domain, new String[]{HGConfig.Classes.HUNTER, HGConfig.Classes.PREY});
    }

    @Override
    public boolean isTrue(State s, String... params) {
        ObjectInstance hunter = s.getObject(params[0]);
        ObjectInstance prey = s.getObject(params[1]);

        int hx = hunter.getIntValForAttribute(HGConfig.Attributes.XPOS);
        int hy = hunter.getIntValForAttribute(HGConfig.Attributes.YPOS);

        int px = prey.getIntValForAttribute(HGConfig.Attributes.XPOS);
        int py = prey.getIntValForAttribute(HGConfig.Attributes.YPOS);

        return (hx == px && hy == py);
    }
}
