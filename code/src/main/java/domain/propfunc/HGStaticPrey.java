package domain.propfunc;


import burlap.oomdp.core.Domain;
import burlap.oomdp.core.PropositionalFunction;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import config.HGConfig;

import java.util.List;

public class HGStaticPrey extends PropositionalFunction {

    public HGStaticPrey(String name, Domain domain) {
        super(name, domain, new String[]{HGConfig.Classes.HUNTER});
    }

    @Override
    public boolean isTrue(State s, String... params) {
        List<ObjectInstance> hunters = s.getObjectsOfClass(HGConfig.Classes.HUNTER);
        for (ObjectInstance hunter : hunters) {
            if(!this.atPrey(hunter)) {
                return false;
            }
        }
        return true;
    }

    private boolean atPrey(ObjectInstance hunter){
        int hx = hunter.getIntValForAttribute(HGConfig.Attributes.XPOS);
        int hy = hunter.getIntValForAttribute(HGConfig.Attributes.YPOS);
        return (hx == 9 && hy == 9);
    }
}
