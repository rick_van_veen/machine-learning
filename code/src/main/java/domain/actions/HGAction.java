package domain.actions;

import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SimpleSGAgentAction;
import config.HGConfig;

public class HGAction extends SimpleSGAgentAction {

    public HGAction(SGDomain d, String name) {
        super(d, name);
    }

    @Override
    public boolean applicableInState(State s, GroundedSGAgentAction agentAction) {
        ObjectInstance actingAgent = s.getObject(agentAction.actingAgent);
        int xPos = actingAgent.getIntValForAttribute(HGConfig.Attributes.XPOS);
        int yPos = actingAgent.getIntValForAttribute(HGConfig.Attributes.YPOS);

        if (agentAction.actionName().equals(HGConfig.Actions.NORTH)) {
            return yPos < (HGConfig.WorldSettings.MAXDIM - 1);
        } else if(agentAction.actionName().equals(HGConfig.Actions.EAST)) {
            return xPos < (HGConfig.WorldSettings.MAXDIM - 1);
        } else if(agentAction.actionName().equals(HGConfig.Actions.SOUTH)) {
            return yPos > (HGConfig.WorldSettings.MINDIM);
        } else { // "west"
            return xPos > (HGConfig.WorldSettings.MINDIM);
        }
    }
}
