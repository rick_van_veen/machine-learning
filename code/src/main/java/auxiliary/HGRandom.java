package auxiliary;

import burlap.debugtools.RandomFactory;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class HGRandom {

    /**
     *
     * Inclusive Min
     * Exclusive Max
     *
     * @param min minimum value, Inclusive
     * @param max maximum value, Exclusive
     * @return A random int in the given range
     */

    public static int randomInt(int min, int max) {
        int maxE = max - 1; //exclusive max
        Random rand =  RandomFactory.getMapped(0);
        return rand.nextInt((maxE - min) + 1) + min;
    }
}
