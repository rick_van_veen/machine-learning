package experiments;

import burlap.debugtools.DPrint;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.WorldObserver;
import config.HGConfig;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HGExperimentAnalysis implements WorldObserver {

    // All data
    List<Map<String, AgentEpisodeData>> agentEpisodeDataList;

    // Curent data map
    Map<String, AgentEpisodeData> episodeDataMap;

    public int debugCode = 31415;

    public HGExperimentAnalysis() {
        this.agentEpisodeDataList = new ArrayList<Map<String, AgentEpisodeData>>();
        this.episodeDataMap = null;
    }

    private void firstGameSetup(State s) {
        this.episodeDataMap = new HashMap<String, AgentEpisodeData>();
        List<ObjectInstance> agentObjectInstances = s.getObjectsOfClass(HGConfig.Classes.HUNTER);
        for (ObjectInstance agentObjecctInstance : agentObjectInstances) {
            this.episodeDataMap.put(agentObjecctInstance.getName(), new AgentEpisodeData());
        }
    }

    public void gameStarting(State s) {
        if (this.episodeDataMap == null) {
            firstGameSetup(s);
        }

        Map<String, AgentEpisodeData> newEpisodeDataMap = new HashMap<String, AgentEpisodeData>();
        for (Map.Entry<String, AgentEpisodeData> episodeDataentry : episodeDataMap.entrySet()) {
            newEpisodeDataMap.put(episodeDataentry.getKey(), episodeDataentry.getValue().copy());
        }

        for (Map.Entry<String, AgentEpisodeData> episodeDataEntry : newEpisodeDataMap.entrySet()) {
            episodeDataEntry.getValue().startEpisode();
        }
        this.episodeDataMap = newEpisodeDataMap;
    }

    public void observe(State s, JointAction ja, Map<String, Double> reward, State sp) {
        for (Map.Entry<String, Double> rewardForAgent : reward.entrySet()) {
            this.episodeDataMap.get(rewardForAgent.getKey()).updateEpisode(rewardForAgent.getValue());
        }
    }

    public void gameEnding(State s) {
        for (Map.Entry<String, AgentEpisodeData> episodeDataEntry : episodeDataMap.entrySet()) {
            episodeDataEntry.getValue().endEpisode(this.agentEpisodeDataList.size());
        }
        this.agentEpisodeDataList.add(this.episodeDataMap);
    }

    public void writeDataToCSV(String filePath) {
        if (!filePath.endsWith(".csv")) {
            filePath = filePath + ".csv";
        }
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(filePath));

            writer.write("episode, agent, numSteps, cumulativeSteps, averageSteps, reward, cumulativeReward, averageReward\r\n");

            for (Map<String, AgentEpisodeData> agentEpisodeMap : this.agentEpisodeDataList) {
                for (Map.Entry<String, AgentEpisodeData> agentEpisodeEntry : agentEpisodeMap.entrySet()) {
                    writer.write(agentEpisodeEntry.getValue().episode + ","
                            + agentEpisodeEntry.getKey().charAt(agentEpisodeEntry.getKey().length() - 1) + ","
                            + agentEpisodeEntry.getValue().getNumSteps() + ","
                            + agentEpisodeEntry.getValue().getCumulativeSteps() + ","
                            + agentEpisodeEntry.getValue().getAverageSteps() + ","
                            + agentEpisodeEntry.getValue().getReward() + ","
                            + agentEpisodeEntry.getValue().getCumulativeReward() + ","
                            + agentEpisodeEntry.getValue().getAverageReward() + "\r\n");
                }
            }
            DPrint.cl(this.debugCode, "Finished writing episode csv file to: " + filePath);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private class AgentEpisodeData {
        // For episode episode
        private int episode;

        private int numSteps;

        private double reward;

        // Over all episodes
        private double cumulativeReward;

        private double averageReward;

        private double cumulativeSteps;

        private double averageSteps;

        public void startEpisode() {
            this.numSteps = 0;
            this.reward = 0.0;
        }

        public void updateEpisode(double reward) {
            this.numSteps++;
            this.reward += reward;
        }

        public void endEpisode(int episode) {
            this.episode = episode;

            this.cumulativeReward += this.reward;
            this.cumulativeSteps += this.numSteps;
            this.averageSteps = this.cumulativeSteps / (double) (this.episode + 1);
            this.averageReward = this.cumulativeReward / (double) (this.episode + 1);
        }

        public int getEpisode() {
            return episode;
        }

        public int getNumSteps() {
            return numSteps;
        }

        public double getReward() {
            return reward;
        }

        public double getCumulativeReward() {
            return cumulativeReward;
        }

        public double getAverageReward() {
            return averageReward;
        }

        public double getCumulativeSteps() {
            return cumulativeSteps;
        }

        public double getAverageSteps() {
            return averageSteps;
        }

        public AgentEpisodeData() {
        }

        public AgentEpisodeData(AgentEpisodeData data) {
            this.episode = data.getEpisode();
            this.numSteps = data.getNumSteps();
            this.reward = data.getReward();
            this.cumulativeReward = data.getCumulativeReward();
            this.averageReward = data.getAverageReward();
            this.cumulativeSteps = data.getCumulativeSteps();
            this.averageSteps = data.getAverageSteps();
        }

        public AgentEpisodeData copy() {
            return new AgentEpisodeData(this);
        }
    }
}
