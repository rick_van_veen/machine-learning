package experiments;

import agents.prey.HGDynamicPrey;
import agents.prey.HGStaticPrey;
import agents.scout.HGDynamicScout;
import agents.types.HGHunterType;
import agents.types.HGPreyType;
import agents.types.HGScoutType;
import auxiliary.HGProgressPrinter;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.behavior.stochasticgames.auxiliary.GameSequenceVisualizer;
import burlap.debugtools.DPrint;
import burlap.domain.stochasticgames.gridgame.GGVisualizer;
import burlap.oomdp.stochasticgames.JointReward;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.common.VisualWorldObserver;
import burlap.oomdp.visualizer.Visualizer;
import config.HGConfig;
import config.HGExperimentConfig;
import domain.HGDomainGenerator;
import domain.HGJointRewardFunctionMultie;
import domain.HGJointRewardFunctionSingle;
import domain.HGTerminalFunction;
import painter.HGHunterPainter;
import painter.HGPreyPainter;
import painter.HGScoutPainter;
import painter.HGVisionAreaPainter;
import state.HGStateAbstraction;
import state.HGStateGenerator;
import world.HGWorld;
import world.HGWorldGenerator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class HGExperiment implements HGIExperiment {

    protected HGWorldGenerator worldGenerator;
    protected HGWorld world;
    protected Visualizer visualizer;
    protected SGDomain domain;
    protected VisualWorldObserver visualWorldObserver;

    HGExperimentConfig experiment;

    public HGExperiment(HGExperimentConfig experiment) {
        this.experiment = experiment;

        initialize();
        addVisualizer();
    }

    protected void initialize() {
        HGDomainGenerator hgDomainGenerator = new HGDomainGenerator(HGConfig.Propositional.HUNTERNEARPREY);
        domain = (SGDomain) hgDomainGenerator.generateDomain();
        HGTerminalFunction tf;
        JointReward rf;
        if(this.experiment.coopTask){
            tf = new HGTerminalFunction(domain.getPropFunction(HGConfig.Propositional.PREYISCAPTUREDCOOP));
            rf = new HGJointRewardFunctionSingle(tf); //all agents get same reward
        }else{
            tf = new HGTerminalFunction(domain.getPropFunction(HGConfig.Propositional.PREYISCAPTUREDSINGLE));
            rf = new HGJointRewardFunctionMultie(tf,domain.getPropFunction(HGConfig.Propositional.HUNTERNEARPREY)); //only agents that captured the prey get a reward
        }
        HGStateGenerator hgStateGenerator = new HGStateGenerator(domain);
        HGStateAbstraction hgStateAbstraction = new HGStateAbstraction(this.experiment.viewDepthHunters, this.experiment.viewDepthScouts);

        // World observer
        List<HGHunterType> hunterTypes = generateHunterTypes();
        List<HGPreyType> preyTypes = generatePreyTypes();
        List<HGScoutType> scoutTypes = generateScoutTypes();


        worldGenerator = new HGWorldGenerator(
                domain, rf, tf, hgStateGenerator, hgStateAbstraction,
                hunterTypes, preyTypes, scoutTypes);
        this.world = worldGenerator.generateWorld();


        DPrint.toggleCode(world.getDebugId(), false);
    }

    protected List<HGHunterType> generateHunterTypes() {
        List<HGHunterType> hunterTypes = new ArrayList<HGHunterType>();

        HGHunterType qhunterType = new HGHunterType(
                this.experiment.learningRate,
                this.experiment.discount,
                this.experiment.epsilon,
                this.experiment.qInit,
                this.experiment.queryOtherAgentsForTheirQValues);

        for (int i = 0; i < this.experiment.numQHunters; i++) {
            hunterTypes.add(qhunterType);
        }

        HGHunterType randomHunterType = new HGHunterType();

        for (int i = 0; i < this.experiment.numRandomHunters; i++) {
            hunterTypes.add(randomHunterType);
        }

        return hunterTypes;
    }

    protected List<HGPreyType> generatePreyTypes() {
        List<HGPreyType> preyTypes = new ArrayList<HGPreyType>();
        HGPreyType staticPreyType = new HGPreyType(HGStaticPrey.class.getName());
        for (int i = 0; i < this.experiment.numStaticPrey; i++) {
            preyTypes.add(staticPreyType);
        }
        HGPreyType dynamicPreyType = new HGPreyType(HGDynamicPrey.class.getName());
        for (int i = 0; i < this.experiment.numDynamicPrey; i++) {
            preyTypes.add(dynamicPreyType);
        }
        return preyTypes;
    }

    protected List<HGScoutType> generateScoutTypes() {
        List<HGScoutType> scoutTypes = new ArrayList<HGScoutType>();
        HGScoutType scoutType = new HGScoutType(HGDynamicScout.class.getName());
        for (int i = 0; i < this.experiment.numScouts; i++) {
            scoutTypes.add(scoutType);
        }
        return scoutTypes;
    }

    public void addVisualizer() {
        // Create the visualizer and add the different painters for the objects
        visualizer = GGVisualizer.getVisualizer(HGConfig.WorldSettings.MAXDIM, HGConfig.WorldSettings.MAXDIM);
        visualizer.addObjectClassPainter(HGConfig.Classes.HUNTER, new HGVisionAreaPainter(this.experiment.viewDepthHunters));
        visualizer.addObjectClassPainter(HGConfig.Classes.SCOUT, new HGVisionAreaPainter(this.experiment.viewDepthScouts));
        visualizer.addObjectClassPainter(HGConfig.Classes.HUNTER, new HGHunterPainter());
        visualizer.addObjectClassPainter(HGConfig.Classes.SCOUT, new HGScoutPainter());
        visualizer.addObjectClassPainter(HGConfig.Classes.PREY, new HGPreyPainter());
    }

    public void showVisualizer(String title){
        // Create observer and add it to the world
        visualWorldObserver = new VisualWorldObserver(domain, visualizer);
        visualWorldObserver.setFrameDelay(this.experiment.frameDelay); //
        visualWorldObserver.initGUI();
        visualWorldObserver.setTitle(title);
        visualWorldObserver.setResizable(false);
        world.addWorldObserver(visualWorldObserver);

        // Disable debug from world
        DPrint.toggleCode(world.getDebugId(), false);
    }

    public void disposeVisualizer(){
        if(visualWorldObserver != null) {
            world.removeWorldObserver(visualWorldObserver);
            visualWorldObserver.setVisible(false);
            visualWorldObserver.dispose();
            visualWorldObserver = null;
        }
    }

    private void runTrainingEpisodes() {

        if(this.experiment.showVisualizerForTraining){
            disposeVisualizer();
            this.showVisualizer("Training");
        }

        HGExperimentAnalysis experimentAnalysis = new HGExperimentAnalysis();
        world.addWorldObserver(experimentAnalysis);

        GameAnalysis episode = null;
        List<GameAnalysis> episodes = new ArrayList<GameAnalysis>();

        System.out.println("--- Training ---");
        for (int i = 0; i < this.experiment.trainingRuns; i++) {
            HGProgressPrinter.printProgress(i,this.experiment.trainingRuns,100);
            episode = world.runTrainingGame(this.experiment.maxStepsPerRun);

            // Add it to the list of games that need to be written to file.
            if (this.experiment.writeVisualizationToFile && i > (1 - this.experiment.percentageOfRunsToFile) * this.experiment.trainingRuns) {
                episodes.add(episode);
            }
        }
        disposeVisualizer();
        writeEpisodesToFile("training", episodes, experimentAnalysis);
    }

    private void writeEpisodesToFile(String id, List<GameAnalysis> games, HGExperimentAnalysis experimentAnalysis) {
        // Write to File
        if (this.experiment.writeToFile) {
            CreateDirsIfnotExists(this.experiment.outputDirectory);
            experimentAnalysis.writeDataToCSV(this.experiment.outputDirectory + this.experiment.statisticFileName + "_" + id +".csv");
        }
        if (this.experiment.writeVisualizationToFile) {
            for (int i = 0; i < games.size(); i++) {
                CreateDirsIfnotExists(this.experiment.outputDirectory + "episodes/" + id);
                games.get(i).writeToFile(this.experiment.outputDirectory + "episodes/" + id  + "/episode" + i);
            }
        }
    }

    private void CreateDirsIfnotExists(String path){
        new File(path).mkdirs();
    }


    private void runTestingEpisodes() {

        if(this.experiment.showVisualizerForTest){
            disposeVisualizer();
            this.showVisualizer("Testing");
        }

        HGExperimentAnalysis experimentAnalysis = new HGExperimentAnalysis();
        world.addWorldObserver(experimentAnalysis);

        GameAnalysis episode = null;
        List<GameAnalysis> episodes = new ArrayList<GameAnalysis>();
        System.out.println("--- Testing ---");
        for (int i = 0; i < this.experiment.testingRuns; i++) {
            HGProgressPrinter.printProgress(i,this.experiment.testingRuns,100);
            episode = world.runTestingGame(this.experiment.maxStepsPerRun);
            if (this.experiment.writeVisualizationToFile && i > (1 - this.experiment.percentageOfRunsToFile) * this.experiment.testingRuns) {
                episodes.add(episode);
            }
        }
        disposeVisualizer();
        writeEpisodesToFile("testing", episodes, experimentAnalysis);
    }

    public void runExperiment() {
        runTrainingEpisodes();
        runTestingEpisodes();
    }

    public void showTestingEpisodes() {
        addVisualizer();
        GameSequenceVisualizer sequenceVisualizer = new GameSequenceVisualizer(
                this.visualizer, domain, this.experiment.outputDirectory + "episodes/testing");
        sequenceVisualizer.setTitle("Testing episodes");
        sequenceVisualizer.initGUI();
    }

    public void showTrainingEpisodes() {
        addVisualizer();
        GameSequenceVisualizer sequenceVisualizer = new GameSequenceVisualizer(
                this.visualizer, domain, this.experiment.outputDirectory + "episodes/training");
        sequenceVisualizer.setTitle("Training episodes");
        sequenceVisualizer.initGUI();
    }
}
