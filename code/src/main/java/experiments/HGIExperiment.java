package experiments;

public interface HGIExperiment {
    void runExperiment();

    void showTestingEpisodes();
    void showTrainingEpisodes();
}
