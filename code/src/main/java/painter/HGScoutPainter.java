package painter;


import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RectangularShape;

public class HGScoutPainter extends HGObjectPainter{

    public HGScoutPainter() {
        super(new Color(0, 116, 187), new Rectangle2D.Float());
    }

    public HGScoutPainter(Color color, RectangularShape shape, int viewDepth) {
        super(color, shape);
    }
}
