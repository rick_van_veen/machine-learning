package painter;

import burlap.oomdp.core.states.State;
import burlap.oomdp.visualizer.StaticPainter;
import config.HGConfig;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RectangularShape;

public class HGStaticPreyPainter implements StaticPainter {

    private Color color;
    private RectangularShape shape;
    private int x;
    private int y;

    public HGStaticPreyPainter(int x, int y) {
        this.color = Color.RED;
        this.shape = new Ellipse2D.Float();
        this.x = x;
        this.y = y;
    }

    public void paint(Graphics2D g2, State s, float cWidth, float cHeight) {
        g2.setColor(this.color);

        //set up floats for the width and height of our domain
        float fWidth = HGConfig.WorldSettings.MAXDIM;
        float fHeight = HGConfig.WorldSettings.MAXDIM;

        //determine the width of a single cell on our canvas
        //such that the whole map can be painted
        float width = cWidth / fWidth;
        float height = cHeight / fHeight;

        //left coordinate of cell on our canvas
        float rx = this.x * width;

        //top coordinate of cell on our canvas
        //coordinate system adjustment because the java canvas
        //origin is in the top left instead of the bottom right
        float ry = cHeight - height - this.y * height;

        // Set the frame for the shape
        this.shape.setFrame(rx, ry, width, height);

        //paint the shape
        g2.fill(this.shape);
    }
}
