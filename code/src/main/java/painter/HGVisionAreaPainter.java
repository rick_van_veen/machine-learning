package painter;

import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.visualizer.ObjectPainter;
import config.HGConfig;

import java.awt.*;
import java.awt.geom.Rectangle2D;

public class HGVisionAreaPainter implements ObjectPainter {

    private int viewDepth;

    public HGVisionAreaPainter(int viewDepth){
        this.viewDepth = viewDepth;
    }

    public void paintObject(Graphics2D g2, State state, ObjectInstance objectInstance, float cWidth, float cHeight) {

        if (viewDepth == 0) {
            return;
        }

        int ax = objectInstance.getIntValForAttribute(HGConfig.Attributes.XPOS);
        int ay = objectInstance.getIntValForAttribute(HGConfig.Attributes.YPOS);

        //set up floats for the width and height of our domain
        float fWidth = HGConfig.WorldSettings.MAXDIM;
        float fHeight = HGConfig.WorldSettings.MAXDIM;

        //determine the width of a single cell on our canvas
        //such that the whole map can be painted
        float width = cWidth / fWidth;
        float height = cHeight / fHeight;

        float lx = (ax - this.viewDepth) * width;
        float ty = cHeight - height - (ay + this.viewDepth) * height;

        g2.setColor(new Color(220, 220, 220));

        float scale = 2 * viewDepth + 1;
        Rectangle2D shape = new Rectangle2D.Float();
        shape.setFrame(lx, ty, scale * width, scale * height);

        g2.fill(shape);

    }
}
