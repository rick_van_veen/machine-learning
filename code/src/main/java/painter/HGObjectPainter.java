package painter;

import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.visualizer.ObjectPainter;
import config.HGConfig;

import java.awt.*;
import java.awt.geom.RectangularShape;

public class HGObjectPainter implements ObjectPainter{

    private Color color;
    private RectangularShape shape;

    public HGObjectPainter(Color color, RectangularShape shape) {
        this.color = color;
        this.shape = shape;
    }

    public void paintObject(Graphics2D g2, State s, ObjectInstance ob, float cWidth, float cHeight) {
        int ax = ob.getIntValForAttribute(HGConfig.Attributes.XPOS);
        int ay = ob.getIntValForAttribute(HGConfig.Attributes.YPOS);

        //set up floats for the width and height of our domain
        float fWidth = HGConfig.WorldSettings.MAXDIM;
        float fHeight = HGConfig.WorldSettings.MAXDIM;

        //determine the width of a single cell on our canvas
        //such that the whole map can be painted
        float width = cWidth / fWidth;
        float height = cHeight / fHeight;


        g2.setColor(this.color);

        //left coordinate of cell on our canvas
        float rx = ax * width;

        //top coordinate of cell on our canvas
        //coordinate system adjustment because the java canvas
        //origin is in the top left instead of the bottom right
        float ry = cHeight - height - ay * height;

        // Set the frame for the shape
        this.shape.setFrame(rx, ry, width, height);

        //paint the rectangle
        g2.fill(this.shape);
    }
}
