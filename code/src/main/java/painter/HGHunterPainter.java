package painter;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RectangularShape;

public class HGHunterPainter extends HGObjectPainter {

    public HGHunterPainter() {
        super(new Color(20, 132, 69), new Rectangle2D.Float());
    }

    public HGHunterPainter(Color color, RectangularShape shape) {
        super(color, shape);
    }
}
