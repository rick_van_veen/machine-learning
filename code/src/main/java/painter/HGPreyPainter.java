package painter;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.RectangularShape;

public class HGPreyPainter extends HGObjectPainter {

    public HGPreyPainter() {
        super(new Color(175, 53, 71), new Ellipse2D.Float());
    }

    public HGPreyPainter(Color color, RectangularShape shape) {
        super(color, shape);
    }
}

