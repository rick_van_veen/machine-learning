package agents.types;

public class HGHunterType {

    private double learningRate;
    private double initialQValue;
    private boolean queryOtherAgentsQValues;
    private double discount;
    private double epsilon;
    private boolean isRandom;

    public HGHunterType(double learningRate, double discount, double epsilon, double initialQValue, boolean queryOtherAgentsQValues) {
        this.learningRate = learningRate;
        this.initialQValue = initialQValue;
        this.queryOtherAgentsQValues = queryOtherAgentsQValues;
        this.discount = discount;
    }

    public HGHunterType(){
        this.isRandom = true;
    }

    public double getInitialQValue() {
        return initialQValue;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public boolean isQueryOtherAgentsQValues() {
        return queryOtherAgentsQValues;
    }

    public double getDiscount() {
        return discount;
    }

    public double getEpsilon() {
        return epsilon;
    }

    public boolean isRandom() {
        return isRandom;
    }
}
