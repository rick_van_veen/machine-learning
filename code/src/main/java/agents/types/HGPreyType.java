package agents.types;

public class HGPreyType {

    private String className;

    public HGPreyType(String className) {
        this.className = className;
    }

    public String getClassName() {
        return className;
    }
}
