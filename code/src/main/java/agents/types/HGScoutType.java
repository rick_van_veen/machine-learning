package agents.types;

public class HGScoutType {

    private String className;

    public HGScoutType(String className) {
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

}
