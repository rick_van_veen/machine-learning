package agents.prey;

import agents.moveables.HGAMoveableObject;
import agents.moveables.HGIMoveableObject;
import burlap.oomdp.core.states.State;

public class HGStaticPrey extends HGAMoveableObject {

    public HGStaticPrey(String worldName, String domainClass) {
        super(worldName, domainClass);
    }

    public void performAction(State s) {
        return; //don't perform any action
    }
}
