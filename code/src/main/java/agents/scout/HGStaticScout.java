package agents.scout;

import agents.moveables.HGAMoveableObject;
import burlap.oomdp.core.states.State;

public class HGStaticScout extends HGAMoveableObject{
    public HGStaticScout(String worldName, String domainClass) {
        super(worldName, domainClass);
    }

    public void performAction(State s) {}
}
