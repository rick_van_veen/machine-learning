package agents.moveables;

import burlap.oomdp.core.states.State;

public interface HGIMoveableObject {
    /**
     *
     * @param s the current state that the prey will modify!
     */
    void performAction(State s);
}
