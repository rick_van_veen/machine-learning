package agents.moveables;

import auxiliary.HGRandom;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import config.HGConfig;

public class HGRandomMovingObject extends HGAMoveableObject{


    public HGRandomMovingObject(String worldName, String domainClass) {
        super(worldName, domainClass);
    }

    public void performAction(State s) {
        ObjectInstance objectInstance = s.getObject(this.worldName);
        computeMove(objectInstance);
    }

    protected void computeMove(ObjectInstance objectInstance) {
        int x = objectInstance.getIntValForAttribute(HGConfig.Attributes.XPOS);
        int y = objectInstance.getIntValForAttribute(HGConfig.Attributes.YPOS);

        int choice = HGRandom.randomInt(0, 4);
        int newX, newY = 0;
        switch(choice) {
            case 0:
                newX = x == (HGConfig.WorldSettings.MAXDIM - 1) ? --x : ++x;
                objectInstance.setValue(HGConfig.Attributes.XPOS, newX);
                break;
            case 1:
                newX = x == HGConfig.WorldSettings.MINDIM ? ++x : --x;
                objectInstance.setValue(HGConfig.Attributes.XPOS, newX);
                break;
            case 2:
                newY = y == (HGConfig.WorldSettings.MAXDIM - 1) ? --y : ++y;
                objectInstance.setValue(HGConfig.Attributes.YPOS, newY);
                break;
            case 3:
                newY = y == HGConfig.WorldSettings.MINDIM ? ++y : --y;
                objectInstance.setValue(HGConfig.Attributes.YPOS, newY);
        }
    }
}
