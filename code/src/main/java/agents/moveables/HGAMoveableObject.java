package agents.moveables;

public abstract class HGAMoveableObject implements HGIMoveableObject{

    protected String worldName;
    protected String domainClass;

    public HGAMoveableObject(String worldName, String domainClass) {
        this.worldName = worldName;
        this.domainClass = domainClass;
    }

    public String getWorldName() {
        return worldName;
    }

    public String getDomainClass() {
        return domainClass;
    }
}
