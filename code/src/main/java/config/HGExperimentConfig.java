package config;

public class HGExperimentConfig {

    // #### Experiment settings #### //

    // Number of training and testing runs
    public final int trainingRuns;
    public final int testingRuns;
    public final int maxStepsPerRun;

    // Data collection
    public final boolean writeToFile;
    public final String statisticFileName;
    public final String outputDirectory;

    // Visualize
    public final boolean showVisualizerForTraining;
    public final boolean showVisualizerForTest;
    public final int frameDelay;

    // Visualization data collection
    public final boolean writeVisualizationToFile;
    public final double percentageOfRunsToFile;

    // #### World settings #### //
    public final int numQHunters;
    public final int viewDepthHunters;
    public final int viewDepthScouts;

    public final int numRandomHunters;
    public final int numScouts;
    public final int numDynamicPrey;
    public final int numStaticPrey;

    public final boolean coopTask;

    // #### QLearning settings #### //
    public final double epsilon;
    public final double discount;
    public final double learningRate;
    public final double qInit;
    public final boolean queryOtherAgentsForTheirQValues;

    public HGExperimentConfig(int trainingRuns, int testingRuns, int maxStepsPerRun, boolean writeToFile, String statisticFileName, String outputDirectory, boolean showVisualizerForTraining, boolean showVisualizerForTest, int frameDelay, boolean writeVisualizationToFile, double percentageOfRunsToFile, int numQHunters, int viewDepthHunters, int viewDepthScouts, int numRandomHunters, int numScouts, int numDynamicPrey, int numStaticPrey, boolean coopTask, double epsilon, double discount, double learningRate, double qInit, boolean queryOtherAgentsForTheirQValues) {
        this.trainingRuns = trainingRuns;
        this.testingRuns = testingRuns;
        this.maxStepsPerRun = maxStepsPerRun;
        this.writeToFile = writeToFile;
        this.statisticFileName = statisticFileName;
        this.outputDirectory = outputDirectory;
        this.showVisualizerForTraining = showVisualizerForTraining;
        this.showVisualizerForTest = showVisualizerForTest;
        this.frameDelay = frameDelay;
        this.writeVisualizationToFile = writeVisualizationToFile;
        this.percentageOfRunsToFile = percentageOfRunsToFile;
        this.numQHunters = numQHunters;
        this.viewDepthHunters = viewDepthHunters;
        this.viewDepthScouts = viewDepthScouts;
        this.numRandomHunters = numRandomHunters;
        this.numScouts = numScouts;
        this.numDynamicPrey = numDynamicPrey;
        this.numStaticPrey = numStaticPrey;
        this.coopTask = coopTask;
        this.epsilon = epsilon;
        this.discount = discount;
        this.learningRate = learningRate;
        this.qInit = qInit;
        this.queryOtherAgentsForTheirQValues = queryOtherAgentsForTheirQValues;
    }
}
