package config;

public class HGConfig {
    public class Classes {
        public static final String HUNTER = "hunter";
        public static final String PREY = "prey";
        public static final String SCOUT = "scout";
    }

    public class Attributes {
        public static final String XPOS = "x";
        public static final String YPOS = "y";
    }

    public class Propositional {
        public static final String HUNTERATPREY = "HunterAtPrey";
        public static final String HUNTERNEARPREY = "HunterNearPrey";
        public static final String PREYISCAPTUREDCOOP = "PreyIsCapturedCoop";
        public static final String PREYISCAPTUREDSINGLE = "PreyIsCapturedSingle";

        public static final String STATICPREY = "StaticPrey";
    }

    public class WorldSettings {
        public static final int MAXDIM = 10;
        public static final int MINDIM = 0;
    }

    public class Actions {
        public static final String NORTH = "north";
        public static final String SOUTH = "south";
        public static final String EAST = "east";
        public static final String WEST = "west";
    }

    public static HGExperimentConfig[] experiments = new HGExperimentConfig[]{

            // #### Single learning agent vs single random agent #### //

            //Experiment 1: Random agent
            new HGExperimentConfig(
                    // #### Experiment settings #### //
                    20000,
                    5000,
                    500,    // max number of steps per episode
                    true,  // write data to file
                    "experiment1", // statistic data file name
                    "output/experiment1/", // output directory
                    false,  // show visualizer for training
                    false,   // show visualizer for testing
                    50,      // frame delay when show visualizer is true
                    true,  // write visualization to file
                    0.1,   // percentage of training and testing runs written to file

                    // #### World settings #### //
                    0,  // number of QHunters
                    2,  // view depth hunters
                    0,  // view depth scouts
                    1,  // number of random hunters
                    0,  // number of scouts
                    1,  // number of dynamic prey
                    0,  // number of static prey
                    true, //coop terminalfunction

                    // #### QLearning settings #### //
                    0.1,    // epsilon value for hunter policy
                    0.9,    // discount factor
                    0.8,    // learning rate
                    0.0,    // initial Q value
                    false    // Share Q values among agents
            ),
            //Experiment 2: Learning agent with view depth 2
            new HGExperimentConfig(
                    // #### Experiment settings #### //
                    20000,   // number of training episodes
                    5000,   // number of testing episodes
                    500,    // max number of steps per episode
                    true,  // write data to file
                    "experiment2", // statistic data file name
                    "output/experiment2/", // output directory
                    false,  // show visualizer for training
                    false,   // show visualizer for testing
                    50,      // frame delay when show visualizer is true
                    true,  // write visualization to file
                    0.1,   // percentage of training and testing runs written to file

                    // #### World settings #### //
                    1,  // number of QHunters
                    2,  // view depth hunters
                    0,  // view depth scouts
                    0,  // number of random hunters
                    0,  // number of scouts
                    1,  // number of dynamic prey
                    0,  // number of static prey
                    true, //coop terminalfunction

                    // #### QLearning settings #### //
                    0.1,    // epsilon value for hunter policy
                    0.9,    // discount factor
                    0.8,    // learning rate
                    0.0,    // initial Q value
                    false    // Share Q values among agents
            ),

            // #### The effect of vision to the learning of a single agent #### //

            //Experiment 3: Learning agent with view depth 3
            new HGExperimentConfig(
                    // #### Experiment settings #### //
                    20000,   // number of training episodes
                    5000,   // number of testing episodes
                    500,    // max number of steps per episode
                    true,  // write data to file
                    "experiment3", // statistic data file name
                    "output/experiment3/", // output directory
                    false,  // show visualizer for training
                    false,   // show visualizer for testing
                    50,      // frame delay when show visualizer is true
                    true,  // write visualization to file
                    0.1,   // percentage of training and testing runs written to file

                    // #### World settings #### //
                    1,  // number of QHunters
                    3,  // view depth hunters
                    0,  // view depth scouts
                    0,  // number of random hunters
                    0,  // number of scouts
                    1,  // number of dynamic prey
                    0,  // number of static prey
                    true, //coop terminalfunction

                    // #### QLearning settings #### //
                    0.1,    // epsilon value for hunter policy
                    0.9,    // discount factor
                    0.8,    // learning rate
                    0.0,    // initial Q value
                    false    // Share Q values among agents
            ),
            //Experiment 4: Learning agent with view depth 4
            new HGExperimentConfig(
                    // #### Experiment settings #### //
                    20000,   // number of training episodes
                    5000,   // number of testing episodes
                    500,    // max number of steps per episode
                    true,  // write data to file
                    "experiment4", // statistic data file name
                    "output/experiment4/", // output directory
                    false,  // show visualizer for training
                    false,   // show visualizer for testing
                    50,      // frame delay when show visualizer is true
                    true,  // write visualization to file
                    0.1,   // percentage of training and testing runs written to file

                    // #### World settings #### //
                    1,  // number of QHunters
                    4,  // view depth hunters
                    0,  // view depth scouts
                    0,  // number of random hunters
                    0,  // number of scouts
                    1,  // number of dynamic prey
                    0,  // number of static prey
                    true, //coop terminalfunction

                    // #### QLearning settings #### //
                    0.1,    // epsilon value for hunter policy
                    0.9,    // discount factor
                    0.8,    // learning rate
                    0.0,    // initial Q value
                    false    // Share Q values among agents
            ),

            // #### Introducing a second hunter: the scout! #### //

            //Experiment 5: Learning agent with random scout both with view depth 2
            new HGExperimentConfig(
                    // #### Experiment settings #### //
                    20000,   // number of training episodes
                    5000,   // number of testing episodes
                    500,    // max number of steps per episode
                    true,  // write data to file
                    "experiment5", // statistic data file name
                    "output/experiment5/", // output directory
                    false,  // show visualizer for training
                    false,   // show visualizer for testing
                    50,      // frame delay when show visualizer is true
                    true,  // write visualization to file
                    0.1,   // percentage of training and testing runs written to file

                    // #### World settings #### //
                    1,  // number of QHunters
                    2,  // view depth hunters
                    2,  // view depth scouts
                    0,  // number of random hunters
                    1,  // number of scouts
                    1,  // number of dynamic prey
                    0,  // number of static prey
                    true,   //coop terminalfunction

                    // #### QLearning settings #### //
                    0.1,    // epsilon value for hunter policy
                    0.9,    // discount factor
                    0.8,    // learning rate
                    0.0,    // initial Q value
                    false    // Share Q values among agents
            ),

            //Experiment 6: Learning agent with 2 random scouts both with view depth 2
            new HGExperimentConfig(
                    // #### Experiment settings #### //
                    20000,   // number of training episodes
                    5000,   // number of testing episodes
                    500,    // max number of steps per episode
                    true,  // write data to file
                    "experiment6", // statistic data file name
                    "output/experiment6/", // output directory
                    false,  // show visualizer for training
                    false,   // show visualizer for testing
                    50,      // frame delay when show visualizer is true
                    true,  // write visualization to file
                    0.1,   // percentage of training and testing runs written to file

                    // #### World settings #### //
                    1,  // number of QHunters
                    2,  // view depth hunters
                    2,  // view depth scouts
                    0,  // number of random hunters
                    2,  // number of scouts
                    1,  // number of dynamic prey
                    0,  // number of static prey
                    true,   //coop terminalfunction

                    // #### QLearning settings #### //
                    0.1,    // epsilon value for hunter policy
                    0.9,    // discount factor
                    0.8,    // learning rate
                    0.0,    // initial Q value
                    false    // Share Q values among agents
            ),

            // #### Multi agent learning with 2 hunters (4/5) #### //

            //Experiment 7: 2 Learning agent with view depth 2
            new HGExperimentConfig(
                    // #### Experiment settings #### //
                    20000,   // number of training episodes
                    5000,   // number of testing episodes
                    500,    // max number of steps per episode
                    true,  // write data to file
                    "experiment7", // statistic data file name
                    "output/experiment7/", // output directory
                    false,  // show visualizer for training
                    false,   // show visualizer for testing
                    50,      // frame delay when show visualizer is true
                    true,  // write visualization to file
                    0.1,   // percentage of training and testing runs written to file

                    // #### World settings #### //
                    2,  // number of QHunters
                    2,  // view depth hunters
                    0,  // view depth scouts
                    0,  // number of random hunters
                    0,  // number of scouts
                    1,  // number of dynamic prey
                    0,  // number of static prey
                    true,   //coop terminalfunction

                    // #### QLearning settings #### //
                    0.1,    // epsilon value for hunter policy
                    0.9,    // discount factor
                    0.8,    // learning rate
                    0.0,    // initial Q value
                    false    // Share Q values among agents
            ),

            // #### Multi agent learning with 2 hunters, hack different terminal function #### //

            //Experiment 8: 2 Learning agent with view depth 2
            new HGExperimentConfig(
                    // #### Experiment settings #### //
                    20000,   // number of training episodes
                    5000,   // number of testing episodes
                    500,    // max number of steps per episode
                    true,  // write data to file
                    "experiment8", // statistic data file name
                    "output/experiment8/", // output directory
                    false,  // show visualizer for training
                    false,   // show visualizer for testing
                    50,      // frame delay when show visualizer is true
                    true,  // write visualization to file
                    0.1,   // percentage of training and testing runs written to file

                    // #### World settings #### //
                    2,  // number of QHunters
                    2,  // view depth hunters
                    0,  // view depth scouts
                    0,  // number of random hunters
                    0,  // number of scouts
                    1,  // number of dynamic prey
                    0,  // number of static prey
                    false,   //coop terminalfunction

                    // #### QLearning settings #### //
                    0.1,    // epsilon value for hunter policy
                    0.9,    // discount factor
                    0.8,    // learning rate
                    0.0,    // initial Q value
                    false    // Share Q values among agents
            ),
    };
}
