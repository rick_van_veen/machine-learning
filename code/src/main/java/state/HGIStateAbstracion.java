package state;

import burlap.oomdp.core.states.State;

// We need our own abstaction interface to provide an abstraction
// per agent and not one abstraction for all agents.
public interface HGIStateAbstracion {
    State abstraction(final State s, final String actingAgentName);
}