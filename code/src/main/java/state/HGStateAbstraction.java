package state;

import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import config.HGConfig;

import java.util.List;

public class HGStateAbstraction implements HGIStateAbstracion {

    private final int viewDepthHunter;
    private final int viewDepthScout;

    public HGStateAbstraction(final int viewDepthHunter, final int viewDepthScout) {
        this.viewDepthHunter = viewDepthHunter;
        this.viewDepthScout = viewDepthScout;
    }

    public State abstraction(final State state, final String actingAgentName) {
        State abstractedState = state.copy();
        ObjectInstance actingAgent = state.getObject(actingAgentName);

        if(!actingAgent.getClassName().equals(HGConfig.Classes.HUNTER)){
            return abstractedState;
        }

        List<ObjectInstance> preyObjects = state.getObjectsOfClass(HGConfig.Classes.PREY);
        List<ObjectInstance> hunterObjects = state.getObjectsOfClass(HGConfig.Classes.HUNTER);
        List<ObjectInstance> scoutObjects = state.getObjectsOfClass(HGConfig.Classes.SCOUT);

        abstractPreys(abstractedState, preyObjects, hunterObjects, scoutObjects);
        abstractHunters(abstractedState, actingAgent);
        abstractedState.removeAllObjects(scoutObjects);
        return abstractedState;
    }

    private void abstractHunters(State abstractedState, ObjectInstance actingHunter) {
        List<ObjectInstance> hunterObjects = abstractedState.getObjectsOfClass(HGConfig.Classes.HUNTER);
        for (ObjectInstance hunterObject : hunterObjects) {
            if (!hunterObject.equals(actingHunter)) {
                hunterObject.setValue(HGConfig.Attributes.XPOS, 0);
                hunterObject.setValue(HGConfig.Attributes.YPOS, 0);
            }
        }
    }

    private void abstractPreys(State abstractedState, List<ObjectInstance> preyObjects, List<ObjectInstance> hunterObjects, List<ObjectInstance> scoutObjects){
        for(ObjectInstance preyObject : preyObjects){
            if(!inSight(preyObject, hunterObjects) && !inSight(preyObject, scoutObjects)) {
                abstractedState.removeObject(preyObject);
            }
        }
    }

    private boolean inSight(ObjectInstance prey, List<ObjectInstance> hunters){
        int xPrey = prey.getIntValForAttribute(HGConfig.Attributes.XPOS);
        int yPrey = prey.getIntValForAttribute(HGConfig.Attributes.YPOS);

        for(ObjectInstance hunter : hunters) {
            int xHunter = hunter.getIntValForAttribute(HGConfig.Attributes.XPOS);
            int yHunter = hunter.getIntValForAttribute(HGConfig.Attributes.YPOS);
            
            int viewDepth = hunter.getClassName().equals(HGConfig.Classes.HUNTER) ? this.viewDepthHunter : this.viewDepthScout;
            if(preyInVDHunter(xPrey, yPrey, xHunter, yHunter, viewDepth)) {
                return true;
            }
        }
        return false;
    }

    private boolean preyInVDHunter(int xPrey, int yPrey, int xHunter, int yhunter, int viewDepth){
        boolean inXFod = (xPrey >= (xHunter - viewDepth)) && (xPrey <= (xHunter + viewDepth));
        return inXFod && ((yPrey <= (yhunter + viewDepth)) && (yPrey >= (yhunter - viewDepth)));
    }
}