package state;

import agents.moveables.HGAMoveableObject;
import auxiliary.HGRandom;
import burlap.oomdp.core.objects.MutableObjectInstance;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.MutableState;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.SGAgent;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.SGStateGenerator;
import config.HGConfig;

import java.util.List;


public class HGStateGenerator extends SGStateGenerator {

    private final SGDomain domain;

    public HGStateGenerator(final SGDomain domain) {this.domain = domain;}

    // TODO: FIX agents should not be on the same location and not on the prey.
    @Override
    public State generateState(List<SGAgent> agents) {
        State state = new MutableState();

        // Add all hunters
        for (SGAgent agent : agents) {
            ObjectInstance agentObj = getAgentObjectInstance(agent);
            setRandomLocationFor(agentObj);

            state.addObject(agentObj);
        }
        return state;
    }

    private ObjectInstance getPreyObjectInstance(HGAMoveableObject moveableObject) {
        return new MutableObjectInstance(domain.getObjectClass(moveableObject.getDomainClass()), moveableObject.getWorldName());
    }

    public State generateState(List<SGAgent> agents, List<HGAMoveableObject> moveableObjects) {
        State state = generateState(agents);

        for (HGAMoveableObject moveable : moveableObjects) {
            ObjectInstance moveableObj = getPreyObjectInstance(moveable);
            setRandomLocationFor(moveableObj);
            state.addObject(moveableObj);
        }
        return state;
    }

    private void setRandomLocationFor(ObjectInstance objectInstance) {
        int x = HGRandom.randomInt(HGConfig.WorldSettings.MINDIM, HGConfig.WorldSettings.MAXDIM);
        int y = HGRandom.randomInt(HGConfig.WorldSettings.MINDIM, HGConfig.WorldSettings.MAXDIM);

        objectInstance.setValue(HGConfig.Attributes.XPOS, x);
        objectInstance.setValue(HGConfig.Attributes.YPOS, y);
    }


}
