clear; clc; close all force;

max_episodes = 20000;
colors = lbmap(2, 'RedBlue');
color_green = [20, 132, 69] ./ 255;

% Training data
% one two or no scouts
% exp5 -> 1 scout
% exp6 -> 2 scouts
% exp2 -> 0 scouts
raw_data_2 = read_csv('./../output/experiment2/experiment2_training.csv');
raw_data_5 = read_csv('./../output/experiment5/experiment5_training.csv');
raw_data_6 = read_csv('./../output/experiment6/experiment6_training.csv');
figure('name', 'Training episodes');
hold on;
plot_average_steps(raw_data_2, max_episodes, colors(1, :), '-');
plot_average_steps(raw_data_5, max_episodes, colors(2, :), '--');
plot_average_steps(raw_data_6, max_episodes, color_green, '-.');
hold off;

axis([0, 20000, 0, 100])

% title('Training episodes');
legend('0 scout', '1 scout', '2 scouts', 'Orientation', 'horizontal' ,'Location', 'northoutside');

hq_plot('Save', sprintf('./../../report/img/%s', 'scout_training.pdf'), ...
    'PaperSize', 598, 'PaperWidthRatio', 0.48, 'PaperWidthHeightRatio', 0.75, 'FontSize', 11);

% Testing data 
max_episodes = 5000;
figure('name', 'Testing episodes');

raw_data_2 = read_csv('./../output/experiment2/experiment2_testing.csv');
raw_data_5 = read_csv('./../output/experiment5/experiment5_testing.csv');
raw_data_6 = read_csv('./../output/experiment6/experiment6_testing.csv');

hold on;
plot_average_steps(raw_data_2, max_episodes, colors(1, :), '-');
plot_average_steps(raw_data_5, max_episodes, colors(2, :), '--');
plot_average_steps(raw_data_6, max_episodes, color_green, '-.');
hold off;

% title('Testing episodes');
legend('0 scout', '1 scout', '2 scouts', 'Orientation', 'horizontal' ,'Location', 'northoutside');

hq_plot('Save', sprintf('./../../report/img/%s', 'scout_testing.pdf'), ...
    'PaperSize', 598, 'PaperWidthRatio', 0.48, 'PaperWidthHeightRatio', 0.75, 'FontSize', 11);

close all force;