clear; clc; close all force;

max_episodes = 20000;
colors = lbmap(2, 'RedBlue');
color_green = [20, 132, 69] ./ 255;

% Training data
% coop vs single
% exp7 -> coop
% exp8 -> single
raw_data_7 = read_csv('./../output/experiment7/experiment7_training.csv');
raw_data_7 = raw_data_7(2:2:end,:);
raw_data_8 = read_csv('./../output/experiment8/experiment8_training.csv');
raw_data_8 = raw_data_8(2:2:end,:);
figure('name', 'Training episodes');
hold on;
plot_average_steps(raw_data_7, max_episodes, colors(1, :), '-');
plot_average_steps(raw_data_8, max_episodes, colors(2, :), '--');
hold off;

axis([0, 20000, 0, 500])

% title('Training episodes');
legend('coop task', 'single task', 'Orientation', 'horizontal' ,'Location', 'northoutside');

hq_plot('Save', sprintf('./../../report/img/%s', 'task_training.pdf'), ...
    'PaperSize', 598, 'PaperWidthRatio', 0.48, 'PaperWidthHeightRatio', 0.75, 'FontSize', 11);

% Testing data 
max_episodes = 5000;
figure('name', 'Testing episodes');

raw_data_7 = read_csv('./../output/experiment7/experiment7_testing.csv');
raw_data_7 = raw_data_7(2:2:end,:);
raw_data_8 = read_csv('./../output/experiment8/experiment8_testing.csv');
raw_data_8 = raw_data_8(2:2:end,:);

hold on;
plot_average_steps(raw_data_7, max_episodes, colors(1, :), '-');
plot_average_steps(raw_data_8, max_episodes, colors(2, :), '--');
hold off;

% title('Testing episodes');
legend('coop task', 'single task', 'Orientation', 'horizontal' ,'Location', 'northoutside');

hq_plot('Save', sprintf('./../../report/img/%s', 'task_testing.pdf'), ...
    'PaperSize', 598, 'PaperWidthRatio', 0.48, 'PaperWidthHeightRatio', 0.75, 'FontSize', 11);

close all force;