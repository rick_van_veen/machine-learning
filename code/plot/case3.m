clear; clc; close all force;

max_episodes = 20000;
colors = lbmap(2, 'RedBlue');
color_red = colors(1, :); 
color_blue = colors(2, :);
color_green = [20, 132, 69] ./ 255;

% Training data
% raw_single_data = read_csv('./../output/experiment2/experiment2_training.csv');
raw_coop_data = read_csv('./../output/experiment7/experiment7_training.csv');
raw_single_task_data = read_csv('./../output/experiment8/experiment8_training.csv');
figure('name', 'Training episodes');
hold on;

plot_average_steps(raw_coop_data, max_episodes, color_red);
plot_average_steps(raw_single_task_data, max_episodes, color_blue);
% plot_average_steps(raw_single_data, max_episodes, color_green);
hold off;

title('Training');

hq_plot('Save', sprintf('./../../presentation/img/%s', 'coop_vs_single_training.png'), ...
    'PaperSize', 720, 'PaperWidthRatio', 0.45, 'PaperWidthHeightRatio', 0.75, 'FontSize', 16, 'Dpi', 300);

% Testing data 
figure('name', 'Testing episodes');
% raw_single_data = read_csv('./../output/experiment2/experiment2_testing.csv');
raw_coop_data = read_csv('./../output/experiment7/experiment7_testing.csv');
raw_single_task_data = read_csv('./../output/experiment8/experiment8_testing.csv');

hold on;
plot_average_steps(raw_coop_data, max_episodes, color_red);
plot_average_steps(raw_single_task_data, max_episodes, color_blue);
% plot_average_steps(raw_single_data, max_episodes, color_green);
hold off;

title('Testing');

hq_plot('Save', sprintf('./../../presentation/img/%s', 'coop_vs_single_testing.png'), ...
    'PaperSize', 720, 'PaperWidthRatio', 0.45, 'PaperWidthHeightRatio', 0.75, 'FontSize', 16, 'Dpi', 300);

close all force;