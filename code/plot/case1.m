%% Random vs Learning
clear; clc; close all force;

max_episodes = 2000;
colors = lbmap(2, 'RedBlue');

% Training data
raw_random_data = read_csv('./../output/experiment1/experiment1_training.csv');
raw_learning_data = read_csv('./../output/experiment2/experiment2_training.csv');
figure('name', 'Training episodes');
hold on;
plot_average_steps(raw_random_data, max_episodes, colors(1, :));
plot_average_steps(raw_learning_data, max_episodes, colors(2, :));
hold off;

title('Training');

hq_plot('Save', sprintf('./../../presentation/img/%s', 'random_vs_learning_single_training.png'), ...
    'PaperSize', 720, 'PaperWidthRatio', 0.45, 'PaperWidthHeightRatio', 0.75, 'FontSize', 16, 'Dpi', 300);

% Testing data 
figure('name', 'Testing episodes');
raw_random_data = read_csv('./../output/experiment1/experiment1_testing.csv');
raw_learning_data = read_csv('./../output/experiment2/experiment2_testing.csv');

hold on;
plot_average_steps(raw_random_data, max_episodes, colors(1, :));
plot_average_steps(raw_learning_data, max_episodes, colors(2, :));
hold off;

title('Testing');

hq_plot('Save', sprintf('./../../presentation/img/%s', 'random_vs_learning_single_testing.png'), ...
    'PaperSize', 720, 'PaperWidthRatio', 0.45, 'PaperWidthHeightRatio', 0.75, 'FontSize', 16, 'Dpi', 300);

close all force;