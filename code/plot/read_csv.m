function [ raw_data ] = read_csv( filepath )
% READ_CSV reads the csv with and returns the raw (matrix) data.

start_row = 1;
start_col = 0;

raw_data = csvread(filepath, start_row, start_col);

end

