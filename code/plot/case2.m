%% Increasing number of scouts 
clear; clc; close all force;

max_episodes = 2000;
colors = lbmap(2, 'RedBlue');
color_red = colors(1, :); 
color_blue = colors(2, :);
color_green = [20, 132, 69] ./ 255;

raw_single_data = read_csv('./../output/experiment2/experiment2_training.csv');
raw_scout_data = read_csv('./../output/experiment5/experiment5_training.csv');
raw_two_scout_data = read_csv('./../output/experiment6/experiment6_learning2_scouts0_random1_training.csv');

figure('name', 'Training episodes');
hold on;
plot_average_steps(raw_single_data, max_episodes, color_green);
plot_average_steps(raw_scout_data, max_episodes, color_red);
plot_average_steps(raw_two_scout_data, max_episodes, color_blue);
hold off;

title('Training');

hq_plot('Save', sprintf('./../../presentation/img/%s', 'effect_of_scouts_training.png'), ...
    'PaperSize', 720, 'PaperWidthRatio', 0.45, 'PaperWidthHeightRatio', 0.75, 'FontSize', 16, 'Dpi', 300);


% Testing data 
figure('name', 'Testing episodes');
raw_single_data = read_csv('./../output/experiment2/experiment2_testing.csv');
raw_scout_data = read_csv('./../output/experiment5/experiment5_testing.csv');
raw_two_scout_data = read_csv('./../output/experiment6/experiment6_testing.csv');

hold on;
plot_average_steps(raw_single_data, max_episodes, color_green);
plot_average_steps(raw_scout_data, max_episodes, color_red);
plot_average_steps(raw_two_scout_data, max_episodes, color_blue);
hold off;

title('Testing');

hq_plot('Save', sprintf('./../../presentation/img/%s', 'effect_of_scouts_testing.png'), ...
    'PaperSize', 720, 'PaperWidthRatio', 0.45, 'PaperWidthHeightRatio', 0.75, 'FontSize', 16, 'Dpi', 300);

close all force;
