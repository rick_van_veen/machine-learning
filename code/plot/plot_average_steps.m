function [ raw_data ] = plot_average_steps( raw_data, max_episodes, color, line_style)
%plot_csv Plot the data from csv file
%   filepath - path to file.

EPISODE_COL = 1;
AVERAGE_STEP_COL = 5;

display((raw_data(end, AVERAGE_STEP_COL)), 'Average cumulative per episode');

plot(raw_data(1:max_episodes, EPISODE_COL), raw_data(1:max_episodes, AVERAGE_STEP_COL),...
    line_style, 'Color', color, 'LineWidth', 2.0);

xlabel('Episode');
ylabel('Average steps (cumulative)');
end

