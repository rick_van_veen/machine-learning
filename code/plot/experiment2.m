clear; clc; close all force;

max_episodes = 20000;
colors = lbmap(2, 'RedBlue');
color_green = [20, 132, 69] ./ 255;

% Training data
raw_data_2 = read_csv('./../output/experiment2/experiment2_training.csv');
raw_data_3 = read_csv('./../output/experiment3/experiment3_training.csv');
raw_data_4 = read_csv('./../output/experiment4/experiment4_training.csv');
figure('name', 'Training episodes');
hold on;
plot_average_steps(raw_data_2, max_episodes, colors(1, :), '-');
plot_average_steps(raw_data_3, max_episodes, colors(2, :), '--');
plot_average_steps(raw_data_4, max_episodes, color_green, '-.');
hold off;

axis([0, 20000, 0, 100])

% title('Training episodes');
legend('vd 2', 'vd 3', 'vd 4', 'Orientation', 'horizontal' ,'Location', 'northoutside');

hq_plot('Save', sprintf('./../../report/img/%s', 'view_depth_training.pdf'), ...
    'PaperSize', 598, 'PaperWidthRatio', 0.48, 'PaperWidthHeightRatio', 0.75, 'FontSize', 11);

% Testing data 
max_episodes = 5000;
figure('name', 'Testing episodes');

raw_data_2 = read_csv('./../output/experiment2/experiment2_testing.csv');
raw_data_3 = read_csv('./../output/experiment3/experiment3_testing.csv');
raw_data_4 = read_csv('./../output/experiment4/experiment4_testing.csv');

hold on;
plot_average_steps(raw_data_2, max_episodes, colors(1, :), '-');
plot_average_steps(raw_data_3, max_episodes, colors(2, :), '--');
plot_average_steps(raw_data_4, max_episodes, color_green, '-.');
hold off;

% title('Testing episodes');
legend('vd 2', 'vd 3', 'vd 4', 'Orientation', 'horizontal' ,'Location', 'northoutside');

hq_plot('Save', sprintf('./../../report/img/%s', 'view_depth_testing.pdf'), ...
    'PaperSize', 598, 'PaperWidthRatio', 0.48, 'PaperWidthHeightRatio', 0.75, 'FontSize', 11);

close all force;