clear; clc; close all force;

max_episodes = 20000;
colors = lbmap(2, 'RedBlue');

% Training data
raw_random_data = read_csv('./../output/experiment1/experiment1_training.csv');
raw_learning_data = read_csv('./../output/experiment2/experiment2_training.csv');
figure('name', 'Training episodes');
hold on;
plot_average_steps(raw_random_data, max_episodes, colors(1, :), '-');
plot_average_steps(raw_learning_data, max_episodes, colors(2, :), '--');
hold off;

% title('Training episodes');
legend('Random', 'Q-Learner', 'Orientation', 'horizontal' ,'Location', 'northoutside');

hq_plot('Save', sprintf('./../../report/img/%s', 'random_vs_learning_single_training.pdf'), ...
    'PaperSize', 598, 'PaperWidthRatio', 0.48, 'PaperWidthHeightRatio', 0.75, 'FontSize', 11);

% Testing data 
max_episodes = 5000;
figure('name', 'Testing episodes');
raw_random_data = read_csv('./../output/experiment1/experiment1_testing.csv');
raw_learning_data = read_csv('./../output/experiment2/experiment2_testing.csv');

hold on;
plot_average_steps(raw_random_data, max_episodes, colors(1, :), '-');
plot_average_steps(raw_learning_data, max_episodes, colors(2, :), '--');
hold off;

% title('Testing episodes');
legend('Random', 'Q-Learner', 'Orientation', 'horizontal' ,'Location', 'northoutside');

hq_plot('Save', sprintf('./../../report/img/%s', 'random_vs_learning_single_testing.pdf'), ...
    'PaperSize', 598, 'PaperWidthRatio', 0.48, 'PaperWidthHeightRatio', 0.75, 'FontSize', 11);

close all force;